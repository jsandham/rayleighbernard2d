!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! RayleighBernard2D 
! def.f90 module
! James Sandham 
! 11 Mar 2014
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


! n - number of grid points in the x direction
! n - number of grid points in the y direction
! Lx - domain size in x direction
! Ly - domain size in y direction
! T - total simulation time
! dt - timestep size
! nt - number of time steps
! plot_freq - plotting frequency
! Re - Reynolds number


module def

  ! MPI constants
  integer, parameter :: root=0
  integer :: ierr,rank,nproc,tt,ind

  ! NetCDF constants
  integer :: ncid, varid

  ! Physical/mathematical constants
  real, parameter :: pi = 3.1415927
  real, parameter :: g = 9.81
  complex, parameter :: ii = (0.0,1.0)

  character(len=4) :: run_type='DNS'
  integer :: plot_freq = 100

  ! Grid/model constants
  integer, parameter :: n = 1024
  integer, parameter :: bs = 1024*256  !n*n/nproc block size
  integer, parameter :: nt = 2000
  real, parameter :: ni = 1.0/n
  real, parameter :: Lx = 2*pi
  real, parameter :: Ly = 2*pi
  real, parameter :: dx = Lx/n
  real, parameter :: dy = Ly/n
  real, parameter :: dy2 = dy**2
  real, parameter :: T = 10.0
  real, parameter :: dt = 0.002    !0.001*(512.0/n)
  real, parameter :: dts = 0.5*dt
  real, parameter :: dtl = 1.5*dt
  real, parameter :: theta = 0.5

  real, parameter :: Re = 1*n  !(2*n/8)**2/(4*pi)
  real, parameter :: hyper = 1
  real, parameter :: Ta = 1.0
  real, parameter :: Tb = 1.0
  real, parameter :: kappa = 1.0/Re

  real :: tstart=0.0
  real :: tfinish=0.0
  real :: telapsed=0.0

end module def


! kappa          dt           N         nt
! 1/128         0.008         64         1250     
! 1/256         0.009         128      1111
! 1/512         0.004         256      2500
! 1/1024        0.002         512      5000
! 1/5215       0.0005        1024      20000 
! 1/6144       0.0003333     1536      35000      22428s on 2 proc
