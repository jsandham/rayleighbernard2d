%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% matlab netcdf reader
% Parallel Navier Stokes 2D
% James Sandham
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear;clc;
n=4; xsize=256; ysize=64; tsize=1;


%create list of netcdf files to be read into matlab
prefix='project'; suffix='.nc'; files={};
for i=1:n
    ni=num2str(i-1);
    if i<=9
        begin='00';
    elseif i<=99
        begin='0';
    end
    new_file=strcat(prefix,begin,ni,suffix);
    files=horzcat(files,new_file);
end
display('files have been read')


for t=1:tsize
    t
    %combine all netcdf files
    DATA=zeros(ysize*n,xsize,1,'single');
    for i=1:n
        data=ncread(files{i},'data',[1,1,t],[ysize,xsize,1]);
        DATA((i-1)*ysize+1:i*ysize,1:xsize,1)=data(:,:,:);
    end
end 

surf(double(DATA),'EdgeColor','none'); 
colormap('gray')
view(0,90);