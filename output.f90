module output
use def
use init
use netcdf
!include 'netcdf.inc'

contains
subroutine create_netcdf_file()
!This is the name of the data file we will create
character(1024) :: tag
character(len=13) :: FILE_NAME

!We are writing 2D data (with 1 time dimension)
integer, parameter :: NDIMS = 3

!When we create netcdf files, variables and dimensions, we get back
!an ID for each one.
integer :: dimids(NDIMS)
integer :: x_dimid, y_dimid, t_dimid

!This is the data array we will write
real, dimension(bs/n,n,nt/plot_freq) :: data_out

!Create output file name
if (rank<=9) then
   write (tag, "(i1)") rank
   FILE_NAME = "project"//"00"//trim(tag)//".nc"
elseif (rank<=99) then
   write (tag, "(i2)") rank
   FILE_NAME = "project"//"0"//trim(tag)//".nc"
else
   write (tag, "(i3)") rank
   FILE_NAME = "project"//trim(tag)//".nc"
endif

!Always check the return code of every netCDF function call. In this
!     example program, wrapping netCDF calls with "call check()"
!     
      call check(nf90_create(FILE_NAME, NF90_CLOBBER, ncid))

      call check(nf90_def_dim(ncid, "x", n, x_dimid))

      call check(nf90_def_dim(ncid, "y", bs/n, y_dimid))

      call check(nf90_def_dim(ncid, "t", nt/plot_freq, t_dimid))

      dimids = (/ y_dimid, x_dimid, t_dimid /)

      call check(nf90_def_var(ncid,"vorticity", NF90_REAL4, dimids, varid))

      call check(nf90_enddef(ncid))

      call check(nf90_put_var(ncid, varid, data_out))

      call check(nf90_close(ncid))

      print *, "*** SUCCESS creating netcdf file! ***"
end subroutine create_netcdf_file 


subroutine write_to_netcdf_file(input,time)
real, dimension(bs), intent(in) :: input
integer, intent(in) :: time

!This is the name of the data file we will write to
character(1024) :: tag
character(len=13) :: FILE_NAME

!This is the data array we will write
real, dimension(bs/n,n,1) :: data_out

!Loop indexes
integer :: i

!Determine file name
if (rank<=9) then
   write (tag, "(i1)") rank
   FILE_NAME = "project"//"00"//trim(tag)//".nc"
elseif (rank<=99) then
   write (tag, "(i2)") rank
   FILE_NAME = "project"//"0"//trim(tag)//".nc"
else
   write (tag, "(i3)") rank
   FILE_NAME = "project"//trim(tag)//".nc"
endif

!Always check the return code of every netCDF function call. In this
!     example program, wrapping netCDF calls with "call check()"
!     

      call check(nf90_open(FILE_NAME,nf90_write,ncid))

      call check(nf90_get_var(ncid, varid, data_out, [1,1,time], [bs/n,n,1]))

      !Fill in data_out array
      do i=1,bs/n
        data_out(i,:,1) = real(input(n*(i-1)+1:n*i))
      enddo

      call check(nf90_put_var(ncid, varid, data_out, [1,1,time], [bs/n,n,1]))

      call check(nf90_close(ncid))

print *, "*** SUCCESS writing to netcdf file! ***"
end subroutine write_to_netcdf_file


subroutine check(status)
integer, intent (in) :: status
if (status /= nf90_noerr) then
   print *, trim(nf90_strerror(status))
   stop 2
endif
end subroutine check



!subroutine create_netcdf_file(input)
!This is the name of the data file we will create
!character(1024) :: tag
!character(len=13) :: FILE_NAME
!real, dimension(bs), intent(in) :: input

!We are writing 2D data (with 1 time dimension)
!integer, parameter :: NDIMS = 3

!When we create netcdf files, variables and dimensions, we get back
!an ID for each one.
!integer :: ncid, varid, dimids(NDIMS)
!integer :: x_dimid, y_dimid, t_dimid

!Loop indexes, and error handling
!integer :: i

!This is the data array we will write
!real, dimension(bs/n,n,1) :: data_out

!Create output file name
!if (rank<=9) then
!   write (tag, "(i1)") rank
!   FILE_NAME = "project"//"00"//trim(tag)//".nc"
!elseif (rank<=99) then
!   write (tag, "(i2)") rank
!   FILE_NAME = "project"//"0"//trim(tag)//".nc"
!else
!   write (tag, "(i3)") rank
!   FILE_NAME = "project"//trim(tag)//".nc"
!endif


!Fill in data_out array
!do i=1,bs/n
!    data_out(i,:,1) = real(input(n*(i-1)+1:n*i))
!enddo

!Always check the return code of every netCDF function call. In this
!     example program, wrapping netCDF calls with "call check()"
!     
!      call check(nf90_create(FILE_NAME, NF90_CLOBBER, ncid)) 
!
!      call check(nf90_def_dim(ncid, "x", n, x_dimid))
!
!      call check(nf90_def_dim(ncid, "y", bs/n, y_dimid))
!
!      call check(nf90_def_dim(ncid, "t", 1, t_dimid))
!
!      dimids = (/ y_dimid, x_dimid, t_dimid /)
!
!      call check(nf90_def_var(ncid,"vorticity", NF90_REAL4, dimids, varid))
!
!      call check(nf90_enddef(ncid))
!
!      call check(nf90_put_var(ncid, varid, data_out))
!
!      call check(nf90_close(ncid))
!
!      print *, "*** SUCCESS writing netcdf file! ***"
!
!end subroutine create_netcdf_file

!subroutine check(status)
!integer, intent (in) :: status
!if (status /= nf90_noerr) then
!   print *, trim(nf90_strerror(status))
!   stop 2
!endif
!end subroutine check


end module output

