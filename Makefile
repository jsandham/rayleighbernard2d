#Makefile for NavierStokes2D CFD code
#define variables
objects= main.o def.o fft.o solvers.o init.o scalar.o diff.o advec.o press.o output.o
F90= mpif90
opt= -O3
prof= -g -pg
execname=main
ARCH      := $(shell uname)  #ifeq ($(ARCH),Linux)
FFTW = /usr/local
NCDF = /usr/local
#MPIP = /home/james/mpiP-3.4

FFTWLIB = -L$(FFTW)/lib -lfftw3f -lm
FFTWINC = -I$(FFTW)/include
NCDFLIB = -L$(NCDF)/lib -lnetcdff -lnetcdf
NCDFINC = -I$(NCDF)/include
#MPIPLIB    = -L$(MPIP)/lib -lmpiP -lm -lunwind


#compile
$(execname): $(objects)
	$(F90) $(opt) $(prof) -o $(execname) $(FFTWINC) $(NCDFINC) $(objects) $(FFTWLIB) $(NCDFLIB)

def.mod: def.o def.f90
	$(F90) $(opt) $(prof) -c def.f90 
fft.mod: fft.o fft.f90
	$(F90) $(opt) $(prof) -c $(FFTWINC) fft.f90
solvers.mod: solvers.o solvers.f90
	$(F90) $(opt) $(prof) -c solvers.f90
init.mod: init.o init.f90
	$(F90) $(opt) $(prof) -c init.f90
scalar.mod: scalar.o scalar.f90
	$(F90) $(opt) $(prof) -c scalar.f90
diff.mod: diff.o diff.f90
	$(F90) $(opt) $(prof) -c diff.f90
advec.mod: advec.o advec.f90
	$(F90) $(opt) $(prof) -c advec.f90
press.mod: press.o press.f90
	$(F90) $(opt) $(prof) -c press.f90
output.mod: output.o output.f90
	$(F90) $(opt) $(prof) -c $(NCDFINC) output.f90


def.o: def.f90
	$(F90) $(opt) $(prof) -c def.f90
fft.o: def.mod fft.f90
	$(F90) $(opt) $(prof) -c $(FFTWINC) fft.f90
solvers.o: def.mod solvers.f90
	$(F90) $(opt) $(prof) -c solvers.f90
init.o: def.mod init.f90
	$(F90) $(opt) $(prof) -c init.f90
scalar.o: def.mod fft.mod scalar.f90
	$(F90) $(opt) $(prof) -c scalar.f90
diff.o: def.mod solvers.mod diff.f90
	$(F90) $(opt) $(prof) -c diff.f90
advec.o: def.mod fft.mod advec.f90
	$(F90) $(opt) $(prof) -c advec.f90
press.o: def.mod fft.mod solvers.mod press.f90
	$(F90) $(opt) $(prof) -c press.f90
output.o: def.mod output.f90
	$(F90) $(opt) $(prof) -c $(NCDFINC) output.f90
main.o: def.mod fft.mod solvers.mod init.mod scalar.mod diff.mod advec.mod press.mod output.mod main.f90
	$(F90) $(opt) $(prof) -c $(FFTWINC) $(NCDFINC)  main.f90 $(FFTWLIB) $(NCDFLIB)


#clean Makefile
clean:
	rm def.mod
	rm fft.mod
	rm solvers.mod
	rm init.mod
	rm scalar.mod
	rm diff.mod
	rm advec.mod
	rm press.mod
	rm output.mod
	rm $(objects)
#end of Makefile

